package com.wang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang
 * @Author: wang fei
 * @CreateTime: 2023-01-26  15:52
 * @Description: TODO
 * @Version: 1.0
 */
@SpringBootApplication
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
