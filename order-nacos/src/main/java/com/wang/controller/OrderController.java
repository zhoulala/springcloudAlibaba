package com.wang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.controllter
 * @Author: wang fei
 * @CreateTime: 2023-01-16  16:48
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/add")
    public String pay(){
        String msg = restTemplate.getForObject("http://stock-nacos/stock/reduct2", String.class);
        return "success" + "  " + msg;
    }
    @GetMapping("/header")
    public String header(@RequestHeader("X-Request-color") String color){
        return color;
    }

    @GetMapping("/AddRequestParameter")
    public String  AddRequestParam(@RequestParam("name") String name){
        return name;
    }

    // 关联流控访问/add 触发/get
    @RequestMapping("/get")
    public String get() throws InterruptedException {
        return "查询订单";
    }
    @RequestMapping("/err")
    public String err()  {
        int a=1/0;
        return "hello";
    }
}
