package com.wang.intercptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

import java.util.UUID;


/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.intercptor
 * @Author: wang fei
 * @CreateTime: 2023-01-20  15:53
 * @Description: TODO
 * @Version: 1.0
 */
public class CustomFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        /* TODO 自定义 feign 请求拦截器  业务逻辑 */
        String access_token= UUID.randomUUID().toString();
        requestTemplate.header("token",access_token);
        System.out.println("请求拦截器feign自定义");

    }
}
