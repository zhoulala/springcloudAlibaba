package com.ribbon;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.netflix.loadbalancer.IRule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.ribbon
 * @Author: wang fei
 * @CreateTime: 2023-01-19  16:41
 * @Description: TODO
 * @Version: 1.0
 */
@Configuration
public class RibbonConfig {
    /**
     * @description: 全局配置 指定负载均衡策略
     * 注意：此处有坑。不能写在@SpringbootApplication注解的@CompentScan扫描得到的地方，否则自定义的配置类就会被所有的RibbonClients共享。不建议这么使用，推荐yml方式
     * 方法名一定和IRule接口中关于负载均衡策略的名称一致
     * @method: iRule
     * @author: wang fei
     * @date: 2023/1/19 16:43:00
     * @param: []
     * @return: com.netflix.loadbalancer.IRule
    **/
    @Bean
    public IRule iRule(){
        //指定使用Nacos提供的负载均衡策略（优先调用同一集群的实例，基于随机权重）
        return new NacosRule();
    }
}
