package com.wang;

import com.ribbon.RibbonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @BelongsProject: SpringCloudAlibabaLearn  @RibbonClient指定微服务及其负载均衡策略。
 * @BelongsPackage: com.wang.controller
 * @Author: wang fei
 * @CreateTime: 2023-01-16  16:54
 * @Description: TODO
 * @Version: 1.0
 */

@SpringBootApplication
@RibbonClients(value = {
        //在SpringBoot主程序扫描的包外定义配置类
        @RibbonClient(name="order-nacos",configuration = RibbonConfig.class)
})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
    @Bean
    @LoadBalanced //负载均衡
    public RestTemplate restTemplate(RestTemplateBuilder  builder){
        RestTemplate restTemplate = builder.build();
        return restTemplate;
    }
}
