package com.wang.controller;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.controllter
 * @Author: wang fei
 * @CreateTime: 2023-01-16  16:48
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @GetMapping("/pay")
//    @SentinelResource(value = "pay",blockHandler = "flowBlockHandler")
    public String pay(){
        return "success";
    }

    @GetMapping("/flowThread")
    @SentinelResource(value = "flowThread",blockHandler = "flowBlockHandler")
    public String flowThread() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        return "success";
    }

    //自定义流控处理返回方法
    public String flowBlockHandler(BlockException e){
        return "流控成功 ,阻塞服务";
    }


    @GetMapping("/add")
    public String add(){
        return "下单成功";
    }

    @GetMapping("/get")
    public String get(){
        return "查询成功";
    }


    /**
     * 热点规则，必须使用@SentinelResource
     * @param id
     * @return
     * @throws InterruptedException
     */
    @RequestMapping("/get/{id}")
    @SentinelResource(value = "getById",blockHandler = "HotBlockHandler")
    public String getById(@PathVariable("id") Integer id) throws InterruptedException {

        System.out.println("正常访问");
        return "正常访问";
    }

    public String HotBlockHandler(@PathVariable("id") Integer id,BlockException e) throws InterruptedException {

        return "热点异常处理";
    }
}
