package com.tulingxueyuan.order.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.tulingxueyuan.order.api
 * @Author: wang fei
 * @CreateTime: 2023-01-25  20:38
 * @Description: TODO
 * @Version: 1.0
 */

@FeignClient(name="alibaba-stock-seata",path = "/stock")
public interface StockService {

    @RequestMapping("/reduct")
    public String reduct(@RequestParam(value="productId") Integer productId);

    @RequestMapping("/find")
    public String find(@RequestParam(value="productId") Integer productId);
}