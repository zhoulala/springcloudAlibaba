package com.wang;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author 飞
 */
@SpringBootApplication
public class StartApplication {
    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class,args);
    }

    /**
     * @description: 注解支持的配置Bean
     * @method: sentinelResourceAspect
     * @author: wang fei
     * @date: 2023/1/22 16:46:26
     * @param: []
     * @return: org.springframework.beans.factory.annotation.Value
    **/
    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }
}
